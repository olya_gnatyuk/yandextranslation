package ru.testing.gateway;

import org.junit.jupiter.api.Test;
import ru.testing.pojo.Response;

import static org.junit.jupiter.api.Assertions.*;

class YandexTranslationGatewayTest {
    private static final String[] TEXTS_TO_TRANSLATE = new String[] {"Hello World!"};
    private static final String[] EXPECTED_TRANSLATIONS = new String[] {"Всем Привет!"};

    private static final String SOURCE_LANGUAGE_CODE = "en";
    private static final String TARGET_LANGUAGE_CODE = "ru";

    @Test
    void getTranslationTest() {
        YandexTranslationGateway yandexTranslationGateway = new YandexTranslationGateway();
        Response response = yandexTranslationGateway.getTranslation(SOURCE_LANGUAGE_CODE,
                TARGET_LANGUAGE_CODE, TEXTS_TO_TRANSLATE);

        assertNotNull(response);
        assertTrue(response.getTranslations().length > 0);
        assertEquals(EXPECTED_TRANSLATIONS[0], response.getTranslations()[0].getText());
    }
}