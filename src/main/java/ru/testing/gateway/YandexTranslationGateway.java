package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.testing.pojo.Response;

@Slf4j
public class YandexTranslationGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "";
    private static final String FOLDER_ID = "";
    private static final String FORMAT = "HTML";

    @SneakyThrows
    public Response getTranslation(String sourceLanguageCode, String targetLanguageCode,
                                   String[] texts) {
        Gson gson = new Gson();
        String text = String.join("\",\"", texts);
        HttpResponse<String> response = Unirest.post(URL)
                .header("Authorization", "Bearer " + TOKEN)
                .header("Accept", "*/*")
                .header("Accept-Charset", "utf-8")
                .header("Content-Type", "text/html; charset=utf-8")
                .body("{\n" +
                        "  \"sourceLanguageCode\": \"" + sourceLanguageCode + "\",\n" +
                        "  \"targetLanguageCode\": \"" + targetLanguageCode + "\",\n" +
                        "  \"format\": \"" + FORMAT + "\",\n" +
                        "  \"folderId\": \"" + FOLDER_ID + "\",\n" +
                        "  \"texts\": [\n" +
                        "  \"" + text + "\" \n" +
                        "  ]\n" +
                        " }\n")
                .asString();
        String strResponse = response.getBody();
        log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Response.class);
    }
}