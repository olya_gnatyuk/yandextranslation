package ru.testing.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Translation {
    @SerializedName("text")
    private String text;
    @SerializedName("detectedLanguageCode")
    private String detectedLanguageCode;
}
