package ru.testing.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Response {
    @SerializedName("translations")
    private Translation[] translations;
}
